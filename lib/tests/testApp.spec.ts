import 'should';
import * as request from 'supertest';
import { Express } from 'express';

import { testApp } from './testApp/service';

const app = testApp;
let server: Express | undefined;

describe('Test App tests', () => {
  before(async () => {
    await app.start();
    server = app.server;
  });

  after(async () => {
    await app.stop();
  });

  describe('POST /api/test', () => {
    const successInput = {
      test: 'string',
      testEnum: 'a',
      testNumber: 1
    };

    it('should return 200 and sample string', async () => {
      const { body } = await request(server)
        .post('/api/test')
        .send({
          ...successInput,
          testBoolean: false,
          testDate: new Date().toISOString(),
          complex: {
            noDecProp: 'test',
            arrayProp: ['test', 'test1', 'test2']
          },
          complexArray: [
            { name: 'test1', test: true },
            { name: 'test2', test: true },
            { name: 'test3', test: false },
            { name: 'test4', test: true },
            { name: 'test5', test: false }
          ]
        })
        .expect(200);

      body.should.be.eql({ a: 11 });
    });

    const validationErrorData = [
      {
        input: {
          ...successInput,
          test: undefined
        },
        details: {
          message: "should have required property 'test'",
          path: '.body'
        }
      },
      {
        input: {
          ...successInput,
          test: []
        },
        details: {
          message: 'should be string',
          path: '.body.test'
        }
      },
      {
        input: {
          ...successInput,
          testEnum: undefined
        },
        details: {
          message: "should have required property 'testEnum'",
          path: '.body'
        }
      },
      {
        input: {
          ...successInput,
          testEnum: 'string'
        },
        details: {
          message: 'should be equal to one of the allowed values',
          path: '.body.testEnum'
        }
      },
      {
        input: {
          ...successInput,
          testNumber: undefined
        },
        details: {
          message: "should have required property 'testNumber'",
          path: '.body'
        }
      },
      {
        input: {
          ...successInput,
          testNumber: 'string'
        },
        details: {
          message: 'should be number',
          path: '.body.testNumber'
        }
      },
      {
        input: {
          ...successInput,
          testNumber: 0
        },
        details: {
          message: 'should be > 0',
          path: '.body.testNumber'
        }
      }
    ];

    validationErrorData.forEach(data => {
      it(`should return 422 ${JSON.stringify(data.details)}`, async () => {
        const { body } = await request(server)
          .post('/api/test')
          .send(data.input)
          .expect(422);

        body.error.message.should.be.eql('Validation error');
        body.error.details.should.be.instanceOf(Array);

        body.error.details[0].message.should.be.eql(data.details.message);
        body.error.details[0].dataPath.should.be.eql(data.details.path);
      });
    });
  });

  describe('GET /api/test', () => {
    const successInput = {
      limit: 10,
      offset: 5
    };

    it('should return 200 and sample string', async () => {
      const { limit, offset } = successInput;

      const { body } = await request(server)
        .get(`/api/test`)
        .query({
          limit,
          offset
        })
        .expect(200);

      body.should.be.eql({ a: 11 });
    });

    const validationErrorData = [
      {
        input: {
          ...successInput,
          limit: undefined
        },
        details: {
          message: 'should be number',
          path: '.query.limit'
        }
      },
      {
        input: {
          ...successInput,
          limit: 400
        },
        details: {
          message: 'should be < 200',
          path: '.query.limit'
        }
      },
      {
        input: {
          ...successInput,
          offset: 'test'
        },
        details: {
          message: 'should be number',
          path: '.query.offset'
        }
      }
    ];

    validationErrorData.forEach(data => {
      it(`should return 422 ${JSON.stringify(data.details)}`, async () => {
        const { limit, offset } = data.input;

        const { body } = await request(server)
          .get(`/api/test?limit=${limit}&offset=${offset}`)
          .expect(422);

        body.error.message.should.be.eql('Validation error');
        body.error.details.should.be.instanceOf(Array);

        body.error.details[0].message.should.be.eql(data.details.message);
        body.error.details[0].dataPath.should.be.eql(data.details.path);
      });
    });
  });

  describe('GET /api/test/:id', () => {
    it('should return 200 and sample string', async () => {
      const { body } = await request(server)
        .get('/api/test/123')
        .expect(200);

      body.should.be.eql({ a: 11 });
    });

    const validationErrorData = {
      input: {
        id: '07485cdd-3ad2-459c-b4ee-3e7580af7f98'
      },
      details: {
        message: 'should be integer',
        path: '.path.id'
      }
    };

    it(`should return 422 ${JSON.stringify(validationErrorData.details)}`, async () => {
      const { body } = await request(server)
        .get(`/api/test/${validationErrorData.input.id}`)
        .expect(422);

      body.error.message.should.be.eql('Validation error');
      body.error.details.should.be.instanceOf(Array);

      body.error.details[0].message.should.be.eql(validationErrorData.details.message);
      body.error.details[0].dataPath.should.be.eql(validationErrorData.details.path);
    });
  });
});
