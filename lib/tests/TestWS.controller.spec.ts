import 'should';
import * as socketClient from 'socket.io-client';

import { testApp } from './testApp/service';

const app = testApp;
let socket: SocketIOClient.Socket | undefined;

describe('Chat Web Socket controller tests', () => {
  before(async () => {
    await app.start();
  });

  afterEach(() => {
    if (socket) {
      socket.close();
      socket = undefined;
    }
  });

  after(async () => {
    await app.stop();
  });

  describe('Socket connection tests', () => {
    it('should connect successfully', done => {
      socket = socketClient.connect('http://127.0.0.1:3030');

      socket.on('connect', function() {
        done();
      });
    });
  });

  describe('Chat operations tests', () => {
    beforeEach(done => {
      socket = socketClient.connect('http://127.0.0.1:3030');

      socket.on('connect', function() {
        done();
      });
    });

    describe('WS Test', () => {
      it('should joined chat by it id', done => {
        socket!.emit('testEvent', { text: 'test' });

        socket!.on('done', (msg: any) => {
          done();
        });
      });
    });
  });
});
