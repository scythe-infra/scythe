import {
  controller,
  description,
  get,
  pathParams,
  post,
  queryParams,
  requestBody,
  response,
  summary
} from '../../routing/decorators';
import { TestInput, TestParams, TestPathParams, TestResponse } from './Test.openapi';
import { TestService } from './Test.service';

@controller('/test', 'Test')
export abstract class TestController {
  @post()
  @summary('Test')
  @description('Test description')
  @response(200, TestResponse)
  public async testPost(@requestBody() body: TestInput) {
    console.log(body);
    return new TestService().getResponse(11);
  }

  @get()
  @description('Test get')
  @summary('Test')
  @response(200, TestResponse)
  public testGet(@queryParams query: TestParams) {
    console.log(query);
    return { a: 11 };
  }

  @get('/:id')
  @summary('Test')
  @description('Test description')
  @response(200, TestResponse)
  public getTest(@pathParams path: TestPathParams) {
    console.log(path);
    return { a: 11 };
  }
}
