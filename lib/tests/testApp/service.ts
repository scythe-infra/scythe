import { TestController } from './Test.controller';
import { ScytheApplicationBuilder } from '../../ScytheApplicationBuilder';
import { TestWSController } from './TestWS.controller';

export const testApp = new ScytheApplicationBuilder()
  .addController(TestController, '/api')
  .addController(TestWSController)
  .build();
