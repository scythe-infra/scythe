import { webSocketController, event, message, socket } from '../../WebSockets/decorators';
import { Socket } from '../../ScytheApplication';
import { WSMessage } from './Test.openapi';

@webSocketController
export abstract class TestWSController {
  @event()
  public testEvent(@message msg: WSMessage, @socket socket: Socket) {
    socket.emit('done');
  }

  @event()
  public disconnect() {
    console.log('user disconnected');
  }
}
