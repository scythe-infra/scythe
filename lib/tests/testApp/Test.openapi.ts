import { example, items, max, min, prop, propEnum, required, Schema, type } from '../../openAPI';

@Schema()
export class TestPathParams {
  @prop
  @required
  @type('integer')
  public id: number;
}

@Schema()
export class TestParams {
  @prop
  @required
  @max(200, true)
  @example(10)
  limit: number;

  @prop @required @min(0) offset: number;
}

@Schema()
export class TestResponse {
  @prop @required @min(10, true) a: number;
}

@Schema()
export class WSMessage {
  @prop
  @required
  public text: string;
}

@Schema({ required: ['test'] })
export class NonRequired {
  @prop test: string;
  @prop data: number;
}

@Schema()
export class RequiredProps extends NonRequired {}

export enum ETestEnum {
  A = 'a',
  B = 'b'
}

@Schema()
class TestNoDecorators {
  @prop noDecProp: string;
  @prop @items('string') arrayProp: string[];
}

@Schema()
class TestArrayItem {
  @prop name: string;
  @prop test: boolean;
}

@Schema()
export class TestInput {
  @prop @required test: string;
  @prop testBoolean: boolean;
  @prop @required @min(0, true) testNumber: number;
  @prop testDate: Date;
  @prop @required @propEnum(ETestEnum) testEnum: ETestEnum;
  @prop complex: TestNoDecorators;
  @prop @items(TestArrayItem) complexArray: TestArrayItem[];
}

// Test start
@Schema()
export class TestModel {
  @prop @items('string') prop1: string[];
  @prop prop2: number;
  @prop prop3: string;
  @prop prop4: boolean;
  @prop prop5: number;
}

@Schema()
export class TestNoDecoratorsComplex extends TestNoDecorators {
  @prop testModel: TestModel;
  @prop testParent: TestInput;
}

@Schema()
export class TestModelList extends TestModel {
  @prop test123: string;
}

@Schema({
  exclude: ['prop2', 'prop3', 'prop4'],
  required: ['prop1']
})
export class NewTestModel extends TestModel {
  @prop @required pidorok: string;
}
