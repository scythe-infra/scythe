import * as Ajv from 'ajv';
import * as valueValidator from 'validator';
import * as _ from 'lodash';
import { dereference } from 'swagger-parser';
import { OpenAPIObject } from 'openapi3-ts';

import { transformCombinedSchema } from './transformCombinedSchema';
import { HttpError } from '../HttpError';
import { OpenAPI } from './OpenAPI';
import { INewable } from './decorators';

export interface IValidatorConfig {
  statusCode?: number;
  pathPrefix?: string;
}

export class SchemaValidator {
  public static openAPI: OpenAPIObject;
  private static ajv = new Ajv({
    allErrors: true,
    nullable: true,
    coerceTypes: true,
    schemaId: 'auto',
    formats: {
      int32: valueValidator.isInt,
      int64: valueValidator.isInt,
      url: valueValidator.isURL
    }
  });

  public static async init(openAPI: OpenAPI) {
    const openAPISpec = openAPI.getSpec();
    const immutableAPIDocs = _.cloneDeep(openAPISpec);
    await dereference(immutableAPIDocs as any);

    this.openAPI = immutableAPIDocs;
  }

  public static getValidationFunction(schema: INewable, config: IValidatorConfig = {}) {
    const openAPISchema = this.openAPI.components!.schemas![schema.name];
    if (!openAPISchema) {
      throw new Error(`Schema with name ${schema.name} was not found`);
    }

    const combinedSchema = transformCombinedSchema(openAPISchema);

    return (data: any) => {
      const isValid = this.ajv.validate(combinedSchema, data);
      if (!isValid && this.ajv.errors) {
        const errors = this.ajv.errors.map(item => ({
          dataPath: (config.pathPrefix || '') + item.dataPath,
          message: item.message,
          params: item.params
        }));

        throw new HttpError(config.statusCode || 422, {
          error: {
            message: 'Validation error',
            details: errors
          }
        });
      }
    };
  }
}
