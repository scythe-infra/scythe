import { IOpenAPIRouterInfo } from '../routing';
import {
  TOpenAPICallbacks,
  TOpenAPIExamples,
  TOpenAPIHeaders,
  TOpenAPILinks,
  TOpenAPIParameters,
  TOpenAPIRequestBodies,
  TOpenAPIResponses,
  TOpenAPISchemas,
  TOpenAPISecuritySchemas
} from './OpenAPI';
import { INewable, PROPS_META, REQUIRED_META, REQUIRED_PROPS_META } from './decorators';

export function buildSchema(schema: INewable) {
  const propsMeta = Reflect.getMetadata(PROPS_META, schema) || new Map();
  const required = Reflect.getOwnMetadata(REQUIRED_META, schema) || [];
  const globalRequired = Reflect.getOwnMetadata(REQUIRED_PROPS_META, schema) || [];
  const propsNames = [...propsMeta.keys()];

  const properties = propsNames.reduce((accum, next) => {
    return {
      ...accum,
      [next]: propsMeta.get(next)
    };
  }, {});

  const allRequiredProps = globalRequired
    .concat(required)
    .filter((value: string, index: number, self: string[]) => self.indexOf(value) === index);

  for (const requiredProp of allRequiredProps) {
    if (!Object.keys(properties).includes(requiredProp)) {
      throw new Error(`Prop ${requiredProp} does not exists on schema ${schema.name}`);
    }
  }

  return {
    type: 'object',
    required: allRequiredProps,
    additionalProperties: false,
    properties
  };
}

export class OpenAPIComponentsBuilder {
  private openAPI: IOpenAPIRouterInfo = {
    schemas: {},
    requestBodies: {},
    responses: {},
    parameters: {},
    examples: {},
    headers: {},
    securitySchemas: {},
    links: {},
    callbacks: {}
  };

  public useOpenAPISchemas(schemas: TOpenAPISchemas): OpenAPIComponentsBuilder {
    this.openAPI.schemas = { ...this.openAPI.schemas, ...schemas };
    return this;
  }

  public useOpenAPIResponses(responses: TOpenAPIResponses): OpenAPIComponentsBuilder {
    this.openAPI.responses = responses;
    return this;
  }

  public registerSchema(schema: INewable): OpenAPIComponentsBuilder {
    this.openAPI.schemas[schema.name] = buildSchema(schema);

    return this;
  }

  public useOpenAPIParameters(parameters: TOpenAPIParameters): OpenAPIComponentsBuilder {
    this.openAPI.parameters = parameters;
    return this;
  }

  public useOpenAPIExamples(examples: TOpenAPIExamples): OpenAPIComponentsBuilder {
    this.openAPI.examples = examples;
    return this;
  }

  public useOpenAPIRequestBodies(requestBodies: TOpenAPIRequestBodies): OpenAPIComponentsBuilder {
    this.openAPI.requestBodies = requestBodies;
    return this;
  }

  public useOpenAPIHeaders(headers: TOpenAPIHeaders): OpenAPIComponentsBuilder {
    this.openAPI.headers = headers;
    return this;
  }

  public useOpenAPISecuritySchemas(securitySchemas: TOpenAPISecuritySchemas): OpenAPIComponentsBuilder {
    this.openAPI.securitySchemas = securitySchemas;
    return this;
  }

  public useOpenAPILinks(links: TOpenAPILinks): OpenAPIComponentsBuilder {
    this.openAPI.links = links;
    return this;
  }

  public useOpenAPICallbacks(callbacks: TOpenAPICallbacks): OpenAPIComponentsBuilder {
    this.openAPI.callbacks = callbacks;
    return this;
  }

  public buildComponents() {
    return this.openAPI;
  }
}
