import {
  CallbackObject,
  ExampleObject,
  HeaderObject,
  LinkObject,
  OpenApiBuilder,
  ParameterObject,
  PathItemObject,
  ReferenceObject,
  RequestBodyObject,
  ResponseObject,
  SchemaObject,
  SecuritySchemeObject
} from 'openapi3-ts'; // @ts-ignore
import { RequestHandler, Router } from 'express';
import { PathParams } from 'express-serve-static-core';
import * as path from 'path';

import { IOpenAPIRouterInfo } from '../routing';

export type TSchemaType = 'string' | 'number' | 'integer' | 'boolean' | 'array' | 'object';

interface IOpenAPIResource {
  path: string;
  spec: PathItemObject;
}

export interface IOpenAPIReferences<Type> {
  [key: string]: Type | ReferenceObject;
}

export type TOpenAPISchemas = IOpenAPIReferences<SchemaObject>;
export type TOpenAPIResponses = IOpenAPIReferences<ResponseObject>;
export type TOpenAPIParameters = IOpenAPIReferences<ParameterObject>;
export type TOpenAPIExamples = IOpenAPIReferences<ExampleObject>;
export type TOpenAPIRequestBodies = IOpenAPIReferences<RequestBodyObject>;
export type TOpenAPIHeaders = IOpenAPIReferences<HeaderObject>;
export type TOpenAPISecuritySchemas = IOpenAPIReferences<SecuritySchemeObject>;
export type TOpenAPILinks = IOpenAPIReferences<LinkObject>;
export type TOpenAPICallbacks = IOpenAPIReferences<CallbackObject>;

export class OpenAPI extends OpenApiBuilder {
  private securityBindings: Map<string, RequestHandler> = new Map<string, RequestHandler>();

  public bindMiddlewareToSecurity(name: string, handler: RequestHandler) {
    this.securityBindings.set(name, handler);
  }

  public getSecurityBinding(name: string) {
    return this.securityBindings.get(name);
  }

  public readResources(router: Router, basePath: PathParams) {
    findAllRoutes(router, '').forEach(route => {
      const transformedResource = {
        path: (basePath === '/' ? '' : basePath) + path.join(route.path).replace(/:([^/]+)/g, '{$1}'),
        spec: route.spec
      };

      super.addPath(transformedResource.path, transformedResource.spec);
    });
  }

  public readComponents(openAPI: IOpenAPIRouterInfo) {
    Object.keys(openAPI).forEach(key => {
      const name = key as keyof IOpenAPIRouterInfo;
      const components = openAPI[name];

      this.addComponents(name, components);
    });
  }

  private addComponents(name: keyof IOpenAPIRouterInfo, components: any) {
    Object.keys(components).forEach(key => {
      this.addComponent(name, key, components[key]);
    });
  }

  private addComponent(name: keyof IOpenAPIRouterInfo, componentName: string, component: any) {
    switch (name) {
      case 'schemas':
        super.addSchema(componentName, component);
        break;
      case 'responses':
        super.addResponse(componentName, component);
        break;
      case 'parameters':
        super.addParameter(componentName, component);
        break;
      case 'examples':
        super.addExample(componentName, component);
        break;
      case 'requestBodies':
        super.addRequestBody(componentName, component);
        break;
      case 'headers':
        super.addHeader(componentName, component);
        break;
      case 'securitySchemas':
        super.addSecurityScheme(componentName, component);
        break;
      case 'links':
        super.addLink(componentName, component);
        break;
      case 'callbacks':
        super.addCallback(componentName, component);
        break;
    }
  }
}

function findAllRoutes(router: Router, routePath: string): IOpenAPIResource[] {
  if (!router || !router.stack || !router.stack.length) {
    return [];
  }

  return Array.prototype.concat.apply(
    [],
    router.stack.map(layer => {
      if (layer.route) {
        if (!layer.route._spec) {
          return [];
        }

        return [
          {
            path: routePath + (layer.path || layer.route.path),
            spec: layer.route._spec
          }
        ];
      }

      return Array.prototype.concat.apply([], findAllRoutes(layer.handle, routePath + (layer.path || '')));
    })
  );
}
