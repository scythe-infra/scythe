export function transformCombinedSchema(combinedSchema: any) {
  if (combinedSchema.properties) {
    for (const key of Object.keys(combinedSchema.properties)) {
      if (combinedSchema.properties[key].type === 'object') {
        combinedSchema.properties[key] = transformCombinedSchema(combinedSchema.properties[key]);
        continue;
      }

      const prop = combinedSchema.properties[key];
      if (prop.oneOf || prop.allOf || prop.anyOf) {
        combinedSchema.properties[key] = transformCombinedSchema(combinedSchema.properties[key]);
      }

      if (combinedSchema.properties[key].type === 'array') {
        combinedSchema.properties[key].items = transformCombinedSchema(combinedSchema.properties[key].items);
        continue;
      }

      if (typeof combinedSchema.properties[key].exclusiveMinimum === 'boolean') {
        const propertyToTransform = combinedSchema.properties[key];
        if (propertyToTransform.exclusiveMinimum === true) {
          propertyToTransform.exclusiveMinimum = propertyToTransform.minimum || 0;
          delete propertyToTransform.minimum;
        } else {
          delete propertyToTransform.exclusiveMinimum;
        }

        combinedSchema.properties[key] = propertyToTransform;
      }

      if (typeof combinedSchema.properties[key].exclusiveMaximum === 'boolean') {
        const propertyToTransform = combinedSchema.properties[key];
        if (propertyToTransform.exclusiveMaximum === true) {
          propertyToTransform.exclusiveMaximum = propertyToTransform.maximum || 0;
          delete propertyToTransform.maximum;
        } else {
          delete propertyToTransform.exclusiveMaximum;
        }

        combinedSchema.properties[key] = propertyToTransform;
      }

      if (combinedSchema.properties[key].format === 'binary') {
        delete combinedSchema.properties[key];
        if (combinedSchema.required && combinedSchema.required.includes(key)) {
          combinedSchema.required = combinedSchema.required.filter((item: string) => item !== key);
        }
      }
    }
  }

  const alternativeSchema: any[] = combinedSchema.oneOf || combinedSchema.allOf || combinedSchema.anyOf;
  if (alternativeSchema) {
    const transformedSchema = alternativeSchema.map(transformCombinedSchema);
    const key = combinedSchema.oneOf ? 'oneOf' : combinedSchema.allOf ? 'allOf' : 'anyOf';
    combinedSchema[key] = transformedSchema;
  }

  return combinedSchema;
}
