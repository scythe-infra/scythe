import { SchemaObject, ContentObject, ReferenceObject, ResponseObject, ParameterObject } from 'openapi3-ts'; // @ts-ignore

interface IOpenAPIResponse {
  [statuscode: string]: ResponseObject | ReferenceObject;
}

interface IContentObject {
  content: ContentObject;
}

export enum EComponentTypes {
  Schemas = 'schemas',
  RequestBodies = 'requestBodies',
  Responses = 'responses',
  Parameters = 'parameters',
  Examples = 'examples',
  Headers = 'headers',
  SecuritySchemas = 'securitySchemas',
  Links = 'links',
  Callbacks = 'callbacks'
}

export function getTypeResponse(statusCode: string, description: string, type: string, isArray: boolean = false) {
  return getResponse(
    statusCode,
    description,
    createSchemaContent(
      isArray
        ? {
            type: 'array',
            items: {
              type
            }
          }
        : { type }
    )
  );
}

function getRefResponse(statusCode: string, description: string, schemaName: string, isArray: boolean = false) {
  return getResponse(statusCode, description, createRefContent(schemaName, isArray));
}

export function createValidationError(dataPath: string, message: string) {
  return {
    error: {
      message: 'Validation error',
      details: [
        {
          dataPath,
          message
        }
      ]
    }
  };
}

export function getRefObject(name: string, type: EComponentTypes = EComponentTypes.Schemas): ReferenceObject {
  return {
    $ref: `#/components/${type}/${name}`
  };
}

export function createSchemaContent(schema: SchemaObject): IContentObject {
  return {
    content: {
      'application/json': {
        schema
      }
    }
  };
}

export function createRefContent(schemaName: string, isArray: boolean = false, contentType?: string): IContentObject {
  return {
    content: {
      [contentType || 'application/json']: {
        schema: isArray
          ? {
              type: 'array',
              items: getRefObject(schemaName)
            }
          : getRefObject(schemaName)
      }
    }
  };
}

export function getResponse(statusCode: string, description: string, content?: IContentObject): IOpenAPIResponse {
  return {
    [statusCode]: {
      description,
      ...content
    }
  };
}

export function getSchemaName(refObject: any): string {
  return refObject.$ref.replace(/^#\/components\/schemas\//, '');
}

export const ValidationErrorResponse: IOpenAPIResponse = getResponse(
  '422',
  'Validation Error',
  createRefContent('ValidationError')
);

export const NotFoundResponse: IOpenAPIResponse = getResponse('404', 'Not found error', createRefContent('BaseError'));

export const UnauthorizedResponse: IOpenAPIResponse = getResponse('401', 'Unauthorized');

export const ForbiddenResponse: IOpenAPIResponse = getResponse('403', 'Forbidden');

export const NoContentResponse: IOpenAPIResponse = getResponse('204', 'No content');

export function CreatedTypeResponse(description: string, type: string, isArray: boolean = false): IOpenAPIResponse {
  return getTypeResponse('201', description, type, isArray);
}

export function CreatedRefResponse(description: string, schemaName: string, isArray: boolean = false): IOpenAPIResponse {
  return getRefResponse('201', description, schemaName, isArray);
}

export function SuccessTypeResponse(description: string, type: string, isArray: boolean = false): IOpenAPIResponse {
  return getTypeResponse('200', description, type, isArray);
}

export function SuccessRefResponse(description: string, schemaName: string, isArray: boolean = false): IOpenAPIResponse {
  return getRefResponse('200', description, schemaName, isArray);
}

export function getQueryParam(name: string, schema: SchemaObject | ReferenceObject, required: boolean = true): ParameterObject {
  return {
    name,
    in: 'query',
    schema,
    required
  };
}
