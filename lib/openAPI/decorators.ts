import 'reflect-metadata';
import { getRefObject } from './openAPIHelpers';
import { SchemaObject } from 'openapi3-ts';
import { TSchemaType, OpenAPI } from './OpenAPI';

export const PROPS_META = Symbol('Props');
export const REQUIRED_META = Symbol('Required');
export const REQUIRED_PROPS_META = Symbol('RequiredProps');
export const SCHEMAS_META = Symbol('SchemaMeta');

export interface INewable {
  new (...args: any): any;
}

export interface ISchemaDecoratorParams {
  exclude?: string[];
  required?: string[];
}

export type TOpenAPIDecorator = (...args: any) => void;

export function Schema(params: ISchemaDecoratorParams = {}): ClassDecorator {
  return target => {
    const schemasMeta = Reflect.getMetadata(SCHEMAS_META, OpenAPI) || new Map();
    const immutableMeta = new Map(schemasMeta);

    immutableMeta.set(target.name, target);

    Reflect.defineMetadata(SCHEMAS_META, immutableMeta, OpenAPI);

    if (params.exclude) {
      const propsMeta = Reflect.getMetadata(PROPS_META, target) || new Map();
      const freshPropsMeta = new Map(propsMeta);

      for (const key of freshPropsMeta.keys()) {
        if (params.exclude.includes(key as string)) {
          freshPropsMeta.delete(key);
        }
      }

      const result = new Map([...freshPropsMeta]);

      Reflect.defineMetadata(PROPS_META, result, target);
    }

    if (params.required) {
      Reflect.defineMetadata(REQUIRED_PROPS_META, params.required, target);
    }
  };
}

function updatePropMeta(key: string | symbol, target: Object, callback: (propMeta: SchemaObject) => SchemaObject) {
  const propsMeta = Reflect.getMetadata(PROPS_META, target.constructor) || new Map();
  const freshMap = new Map(propsMeta);

  let propMeta: any = freshMap.get(key) || {};
  propMeta = callback(propMeta);
  freshMap.set(key, propMeta);

  Reflect.defineMetadata(PROPS_META, freshMap, target.constructor);
}

export function required(target: Object, propertyKey: string | symbol) {
  const requiredMeta = Reflect.getMetadata(REQUIRED_META, target.constructor) || [];
  const targetMeta = requiredMeta.concat([propertyKey]);
  Reflect.defineMetadata(REQUIRED_META, targetMeta, target.constructor);
}

export function items(type: string | INewable): PropertyDecorator {
  return (target, propertyKey) => {
    updatePropMeta(propertyKey, target, propMeta => {
      if (typeof type === 'string') {
        propMeta.items = {
          type
        };
      } else {
        propMeta.items = getRefObject(type.name);
      }

      return propMeta;
    });
  };
}

export function prop(target: Object, propertyKey: string | symbol) {
  updatePropMeta(propertyKey, target, propMeta => {
    const type = Reflect.getMetadata('design:type', target, propertyKey);
    const typeName = type ? type.name : 'Object';

    switch (typeName) {
      case 'Number':
      case 'String':
      case 'Boolean':
      case 'Array':
        propMeta.type = propMeta.type || typeName.toLowerCase();
        break;
      case 'Object':
        propMeta.type = 'object';
        break;
      case 'Date':
        propMeta.format = 'date-time';
        propMeta.type = 'string';
        break;
      default:
        propMeta = getRefObject(typeName);
    }

    return propMeta;
  });
}

export function schema(schema: SchemaObject): PropertyDecorator {
  return (target, propertyKey) => {
    updatePropMeta(propertyKey, target, propMeta => {
      return { ...propMeta, ...schema };
    });
  };
}

export function type(type: TSchemaType): PropertyDecorator {
  return (target, propertyKey) => {
    updatePropMeta(propertyKey, target, propMeta => {
      propMeta.type = type;
      return propMeta;
    });
  };
}

export function propDescription(description: string): PropertyDecorator {
  return (target, propertyKey) => {
    updatePropMeta(propertyKey, target, propMeta => {
      propMeta.description = description;
      return propMeta;
    });
  };
}

export function format(format: string): PropertyDecorator {
  return (target, propertyKey) => {
    updatePropMeta(propertyKey, target, propMeta => {
      propMeta.format = format;
      return propMeta;
    });
  };
}

export function max(value: number, exclusive: boolean = false): PropertyDecorator {
  return (target, propertyKey) => {
    updatePropMeta(propertyKey, target, propMeta => {
      propMeta.maximum = value;
      propMeta.exclusiveMaximum = exclusive;
      return propMeta;
    });
  };
}

export function example(example: any): PropertyDecorator {
  return (target, propertyKey) => {
    updatePropMeta(propertyKey, target, propMeta => {
      propMeta.example = example;
      return propMeta;
    });
  };
}

export function combineOpenAPIDecorators(...decorators: TOpenAPIDecorator[]): PropertyDecorator {
  return (target, propertyKey) => {
    return decorators.forEach(dec => {
      dec(target, propertyKey);
    });
  };
}

export function min(value: number, exclusive: boolean = false): PropertyDecorator {
  return (target, propertyKey) => {
    updatePropMeta(propertyKey, target, propMeta => {
      propMeta.minimum = value;
      propMeta.exclusiveMinimum = exclusive;
      return propMeta;
    });
  };
}

export function pattern(pattern: string): PropertyDecorator {
  return (target, propertyKey) => {
    updatePropMeta(propertyKey, target, propMeta => {
      propMeta.pattern = pattern;
      return propMeta;
    });
  };
}

export function maxLength(length: number): PropertyDecorator {
  return (target, propertyKey) => {
    updatePropMeta(propertyKey, target, propMeta => {
      propMeta.maxLength = length;
      return propMeta;
    });
  };
}

export function minLength(length: number): PropertyDecorator {
  return (target, propertyKey) => {
    updatePropMeta(propertyKey, target, propMeta => {
      propMeta.minLength = length;
      return propMeta;
    });
  };
}

export function minItems(amount: number): PropertyDecorator {
  return (target, propertyKey) => {
    updatePropMeta(propertyKey, target, propMeta => {
      propMeta.minItems = amount;
      return propMeta;
    });
  };
}

export function maxItems(amount: number): PropertyDecorator {
  return (target, propertyKey) => {
    updatePropMeta(propertyKey, target, propMeta => {
      propMeta.maxItems = amount;
      return propMeta;
    });
  };
}

export function uniqueItems(target: Object, propertyKey: string | symbol) {
  updatePropMeta(propertyKey, target, propMeta => {
    propMeta.uniqueItems = true;
    return propMeta;
  });
}

export function propEnum(enumValues: object): PropertyDecorator {
  return (target, propertyKey) => {
    updatePropMeta(propertyKey, target, propMeta => {
      const values = Object.keys(enumValues)
        .filter(value => isNaN(Number(value)))
        .map(key => (enumValues as any)[key]);

      propMeta.enum = values;
      propMeta.type = typeof values[0];

      return propMeta;
    });
  };
}

export function oneOf(...models: INewable[]): PropertyDecorator {
  return (target, propertyKey) => {
    updatePropMeta(propertyKey, target, propMeta => {
      propMeta.oneOf = models.map(model => getRefObject(model.name));
      return propMeta;
    });
  };
}

export function allOf(...models: INewable[]): PropertyDecorator {
  return (target, propertyKey) => {
    updatePropMeta(propertyKey, target, propMeta => {
      propMeta.oneOf = models.map(model => getRefObject(model.name));
      return propMeta;
    });
  };
}

export function anyOf(...models: INewable[]): PropertyDecorator {
  return (target, propertyKey) => {
    updatePropMeta(propertyKey, target, propMeta => {
      propMeta.oneOf = models.map(model => getRefObject(model.name));
      return propMeta;
    });
  };
}
