import 'reflect-metadata';

export const TAG_WS_CONTROLLER = Symbol('WebSocketController');

export function webSocketController(Controller: Function) {
  Reflect.defineMetadata(TAG_WS_CONTROLLER, {}, Controller);
}
