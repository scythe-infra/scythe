import 'reflect-metadata';
import { Server, Socket } from 'socket.io';

export type TEventHandler = (message: any, socket: Socket, server: Server) => void | Promise<void>;

export const TAG_WS_EVENTS = Symbol('WebSocketEvents');
export const TAG_WS_EVENT_NAME = Symbol('WebSocketEventName');

export function event(eventName?: string): MethodDecorator {
  return function(target: Object, key: string | symbol) {
    const methodsMeta: Map<string, Function> = Reflect.getMetadata(TAG_WS_EVENTS, target.constructor) || new Map();

    const newMethodsMeta = new Map(methodsMeta);
    const stringKey = typeof key === 'string' ? key : key.toString();

    Reflect.defineMetadata(TAG_WS_EVENT_NAME, eventName || stringKey, target.constructor, key);

    const handler: Function = (target as any)[key];
    newMethodsMeta.set(stringKey, handler);

    Reflect.defineMetadata(TAG_WS_EVENTS, newMethodsMeta, target.constructor);
  };
}
