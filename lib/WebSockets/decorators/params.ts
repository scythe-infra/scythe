import { getRefObject } from '../../openAPI';

export const TAG_WS_METHOD_PARAMS = Symbol('WsMethodParams');

export enum EWSMethodParams {
  Socket = 'socket',
  Server = 'server',
  Message = 'message'
}

export interface IWSMethodParams {
  type: EWSMethodParams;
  index: number;
  schema?: any;
}

function setWSMethodsPropertyMeta(
  target: Object,
  propertyKey: string | symbol,
  parameterIndex: number,
  type: EWSMethodParams,
  schemaName?: string
) {
  const methodParamsMeta = Reflect.getMetadata(TAG_WS_METHOD_PARAMS, target.constructor, propertyKey) || [];
  const immutableMethodParamsMeta: IWSMethodParams[] = methodParamsMeta.concat([]);

  immutableMethodParamsMeta.push({
    type,
    index: parameterIndex,
    schema: schemaName ? getRefObject(schemaName) : undefined
  });

  Reflect.defineMetadata(TAG_WS_METHOD_PARAMS, immutableMethodParamsMeta, target.constructor, propertyKey);
}

export function socket(target: Object, propertyKey: string, parameterIndex: number) {
  setWSMethodsPropertyMeta(target, propertyKey, parameterIndex, EWSMethodParams.Socket);
}

export function server(target: Object, propertyKey: string, parameterIndex: number) {
  setWSMethodsPropertyMeta(target, propertyKey, parameterIndex, EWSMethodParams.Server);
}

export function message(target: Object, propertyKey: string, parameterIndex: number) {
  const params = Reflect.getMetadata('design:paramtypes', target, propertyKey);
  const querySchema = params[parameterIndex];

  setWSMethodsPropertyMeta(target, propertyKey, parameterIndex, EWSMethodParams.Message, querySchema.name);
}
