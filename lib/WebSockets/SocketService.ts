import { WebSocketServer } from '../ScytheApplication';

export class SocketService {
  public static server?: WebSocketServer;

  public static init(server: WebSocketServer) {
    this.server = server;
  }

  public static emit(eventType: string, message: any, room?: string) {
    if (!this.server) {
      throw new Error('You must call init first');
    }

    if (!room) {
      this.server.emit(eventType, message);
    } else {
      this.server.to(room).emit(eventType, message);
    }
  }
}
