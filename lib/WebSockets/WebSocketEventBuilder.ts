import { EWSMethodParams, TAG_WS_EVENT_NAME, TAG_WS_EVENTS, TAG_WS_METHOD_PARAMS, TEventHandler } from './decorators';
import { getSchemaName, OpenAPI, SCHEMAS_META, SchemaValidator } from '../openAPI';
import { Logger } from 'log4js';

export class WebSocketEventBuilder {
  private eventsMeta: Map<string, Function>;
  private logger?: Logger;

  constructor(private Controller: any) {
    // const wsControllerMeta = Reflect.getMetadata(TAG_WS_CONTROLLER, Controller);
    const eventsMeta: Map<string, Function> | undefined = Reflect.getMetadata(TAG_WS_EVENTS, this.Controller);

    if (!eventsMeta) {
      throw new Error('You did not specify any events for WS Controller');
    }

    this.eventsMeta = eventsMeta;
  }

  public setLogger(logger: Logger): WebSocketEventBuilder {
    this.logger = logger;
    return this;
  }

  public build(events: Map<string, TEventHandler>): Map<string, TEventHandler> {
    const wsEvents = new Map();
    for (const eventKey of this.eventsMeta.keys()) {
      const eventHandler = this.buildHandler(eventKey);
      const eventName = this.getEventName(eventKey, events);

      wsEvents.set(eventName, eventHandler);
    }

    return wsEvents;
  }

  private getEventName(eventKey: string, events: Map<string, TEventHandler>) {
    const eventName = Reflect.getMetadata(TAG_WS_EVENT_NAME, this.Controller, eventKey);
    if (events.get(eventName)) {
      throw new Error(`Event with ${eventName} name already exist`);
    }

    return eventName;
  }

  private buildHandler(eventKey: string): TEventHandler {
    const eventHandler = this.eventsMeta.get(eventKey);

    if (!eventHandler) {
      throw new Error(`Event handler ${eventKey} is defined`);
    }

    const eventParams = Reflect.getMetadata(TAG_WS_METHOD_PARAMS, this.Controller, eventKey) || [];
    const messageParam = eventParams.find((item: any) => item.type === EWSMethodParams.Message);

    let messageValidator: Function;

    if (messageParam) {
      const schemasMeta = Reflect.getMetadata(SCHEMAS_META, OpenAPI) || new Map();
      const messageSchemaName = getSchemaName(messageParam.schema);
      const messageSchema = schemasMeta.get(messageSchemaName);

      if (!messageSchema) {
        throw new Error(`Schema ${messageSchemaName} is not defined`);
      }

      messageValidator = SchemaValidator.getValidationFunction(messageSchema);
    }
    return async (message, socket, server) => {
      const params = [];
      for (const paramMeta of eventParams) {
        switch (paramMeta.type) {
          case EWSMethodParams.Server:
            params[paramMeta.index] = server;
            break;
          case EWSMethodParams.Socket:
            params[paramMeta.index] = socket;
            break;
          case EWSMethodParams.Message:
            params[paramMeta.index] = message;
            break;
        }
      }
      try {
        if (messageValidator) {
          messageValidator(message);
        }

        await eventHandler(...params);
      } catch (err) {
        if (this.logger) {
          this.logger.error('Request error', err);
        }
        socket.emit('requestError', err);
      }
    };
  }
}
