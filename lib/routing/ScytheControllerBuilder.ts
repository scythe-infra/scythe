import {
  EMethodParam,
  IMethodParams,
  TAG_BASE_SECURITY,
  TAG_COMMON_MIDDLEWARES,
  TAG_CONTROLLER,
  TAG_CONTROLLER_ENV,
  TAG_DISABLE_VALIDATION,
  TAG_METHOD,
  TAG_METHOD_ENV,
  TAG_METHODS_PARAMS,
  TAG_MIDDLEWARES,
  TAG_OPENAPI
} from './decorators';
import { ScytheRouterBuilder } from './ScytheRouterBuilder';
import { getSchemaHydrator } from '../hydrators';

import { getSchemaName, IValidatorConfig, OpenAPI, SCHEMAS_META, SchemaValidator, UnauthorizedResponse } from '../openAPI';
import { NextFunction, Request, RequestHandler, Response } from 'express';
import { OperationObject } from 'openapi3-ts';

interface IControllerMethodInfo {
  method: string;
  handlers: RequestHandler[];
  docs: OperationObject;
}

export class ScytheControllerBuilder {
  private controllerEnv: string[];
  private securitySchema: string;
  private controllerMeta: { tag?: string; path: string };
  private commonMiddlewares: RequestHandler[];
  private openAPI?: OpenAPI;
  private config?: any;

  constructor(private Controller: any, private basePath: string = '') {
    this.controllerEnv = Reflect.getMetadata(TAG_CONTROLLER_ENV, this.Controller) || [];
    this.securitySchema = Reflect.getMetadata(TAG_BASE_SECURITY, Controller);
    this.controllerMeta = Reflect.getMetadata(TAG_CONTROLLER, Controller);
    this.commonMiddlewares = Reflect.getMetadata(TAG_COMMON_MIDDLEWARES, Controller) || [];
  }

  public build() {
    const allMethods = Reflect.getMetadata(TAG_METHOD, this.Controller) || new Map();
    const paths = [...allMethods.keys()];

    const builder = new ScytheRouterBuilder();

    for (const path of paths) {
      const fullPath = this.basePath + this.controllerMeta.path + (path === '/' ? '' : path);
      const methodsInfo: IControllerMethodInfo[] = [];

      const methods = allMethods.get(path);
      for (let [methodName, methodInfo] of methods) {
        const methodMeta = this.buildMethod(methodName, methodInfo.key, methodInfo.handle);
        if (methodMeta) {
          methodsInfo.push(methodMeta);
        }
      }

      if (methodsInfo.length) {
        builder.useNamespace(fullPath);
        builder.useOpenAPIDocs(
          methodsInfo.reduce((prev, next) => {
            return {
              ...prev,
              [next.method]: next.docs
            };
          }, {})
        );
        for (const methodInfo of methodsInfo) {
          (builder.buildNamespace() as any)[methodInfo.method](...methodInfo.handlers);
        }
      }
    }

    return builder.buildRouter();
  }

  public setConfig(config: any): ScytheControllerBuilder {
    this.config = config;
    return this;
  }

  public setOpenAPI(openAPI: OpenAPI): ScytheControllerBuilder {
    this.openAPI = openAPI;
    return this;
  }

  private buildMethod(methodName: string, methodKey: string, handler: Function) {
    const methodEnv = Reflect.getMetadata(TAG_METHOD_ENV, this.Controller, methodKey) || [];
    const totalEnv = this.controllerEnv.concat(methodEnv);
    if (totalEnv.length && !totalEnv.includes(process.env.NODE_ENV || '')) {
      return undefined;
    }

    const methodDocs = Reflect.getMetadata(TAG_OPENAPI, this.Controller, methodKey) || { responses: {} };

    if (this.controllerMeta.tag && !methodDocs.tags) {
      methodDocs.tags = [this.controllerMeta.tag];
    }

    if (this.securitySchema && !methodDocs.security) {
      methodDocs.security = [
        {
          [this.securitySchema]: []
        }
      ];
      methodDocs.responses = {
        ...methodDocs.responses,
        ...UnauthorizedResponse
      };
    }

    const authMiddlewares = this.configureSecurity(methodDocs);
    const methodMiddlewares = Reflect.getMetadata(TAG_MIDDLEWARES, this.Controller, methodKey) || [];
    const finalMiddleware = [this.buildFinalMiddleware(methodKey, handler, methodDocs)];
    const middlewares = [...authMiddlewares, ...this.commonMiddlewares, ...methodMiddlewares, ...finalMiddleware];

    return {
      method: methodName,
      handlers: middlewares,
      docs: methodDocs
    };
  }

  private configureSecurity(methodDocs: any) {
    if (!this.openAPI) {
      throw new Error('You must specify OpenAPI');
    }

    const authMiddlewares = [];
    for (const securityObject of methodDocs.security || []) {
      const schemaKey = Object.keys(securityObject)[0];
      const securityMiddleware = this.openAPI.getSecurityBinding(schemaKey);
      if (securityMiddleware) {
        authMiddlewares.push(securityMiddleware);
      }
    }

    return authMiddlewares;
  }

  private buildSchemaValidator(refObject: any, params: IValidatorConfig) {
    const reference = refObject.type === 'array' ? refObject.items : refObject;

    const schemasMeta = Reflect.getMetadata(SCHEMAS_META, OpenAPI) || new Map();
    const schemaName = getSchemaName(reference);
    const RequestBodySchema = schemasMeta.get(schemaName);

    const validator = SchemaValidator.getValidationFunction(RequestBodySchema, params);
    return refObject.type === 'array' ? (data: any[]) => data.forEach(validator) : validator;
  }

  private getFinalMiddlewareParams(methodParams: IMethodParams[], isValidationDisabled: boolean) {
    if (!this.config) {
      throw new Error('Yous must specify config');
    }

    const { hydrateOpenAPIModels, validateRequests } = this.config;

    const bodyParam = methodParams.find(item => item.type === EMethodParam.Body);
    const pathParam = methodParams.find(item => item.type === EMethodParam.Path);
    const queryParam = methodParams.find(item => item.type === EMethodParam.Query);

    const bodyHydrator = bodyParam && hydrateOpenAPIModels ? getSchemaHydrator(bodyParam.schema) : undefined;
    const pathHydrator = pathParam && hydrateOpenAPIModels ? getSchemaHydrator(pathParam.schema) : undefined;
    const queryHydrator = queryParam && hydrateOpenAPIModels ? getSchemaHydrator(queryParam.schema) : undefined;

    const bodyValidator =
      bodyParam && validateRequests && !isValidationDisabled
        ? this.buildSchemaValidator(bodyParam.schema, { pathPrefix: '.body' })
        : undefined;
    const pathValidator =
      pathParam && validateRequests && !isValidationDisabled
        ? this.buildSchemaValidator(pathParam.schema, { pathPrefix: '.path' })
        : undefined;
    const queryValidator =
      queryParam && validateRequests && !isValidationDisabled
        ? this.buildSchemaValidator(queryParam.schema, { pathPrefix: '.query' })
        : undefined;

    return (req: Request, res: Response, next: NextFunction) => {
      queryValidator && queryValidator(req.query);
      bodyValidator && bodyValidator(req.body);
      pathValidator && pathValidator(req.params);

      const params = [];
      for (const paramMeta of methodParams) {
        switch (paramMeta.type) {
          case EMethodParam.Body:
            params[paramMeta.index] = bodyHydrator ? bodyHydrator(req.body) : req.body;
            break;
          case EMethodParam.Next:
            params[paramMeta.index] = next;
            break;
          case EMethodParam.Path:
            params[paramMeta.index] = pathHydrator ? pathHydrator(req.params) : req.params;
            break;
          case EMethodParam.Query:
            params[paramMeta.index] = queryHydrator ? queryHydrator(req.query) : req.query;
            break;
          case EMethodParam.Req:
            params[paramMeta.index] = req;
            break;
          case EMethodParam.Res:
            params[paramMeta.index] = res;
            break;
          case EMethodParam.User:
            params[paramMeta.index] = (req as any).user;
            break;
        }
      }

      return params;
    };
  }

  private buildResponseValidator(responses: any) {
    if (!this.config) {
      throw new Error('Yous must specify config');
    }

    const { validateResponse } = this.config;
    const response = Object.keys(responses).find(code => parseInt(code) >= 200 && parseInt(code) < 300 && parseInt(code) !== 204);

    if (validateResponse && response) {
      const responseContent = responses[response].content;
      const schemaRef = responseContent[Object.keys(responseContent)[0]].schema;

      return this.buildSchemaValidator(schemaRef, { statusCode: 500 });
    }

    return undefined;
  }

  private buildFinalMiddleware(methodKey: string, handler: Function, methodDocs: any) {
    const methodParams: IMethodParams[] = Reflect.getMetadata(TAG_METHODS_PARAMS, this.Controller, methodKey) || [];
    const isValidationDisabled = Reflect.getMetadata(TAG_DISABLE_VALIDATION, this.Controller, methodKey);

    const responseCode = Object.keys(methodDocs.responses).find(code => parseInt(code) >= 200 && parseInt(code) < 300);
    const redirectCode = Object.keys(methodDocs.responses).find(code => parseInt(code) >= 300 && parseInt(code) < 400);

    if (!responseCode && !redirectCode) {
      throw new Error('Success response is not described');
    }
    const paramsBuilder = this.getFinalMiddlewareParams(methodParams, isValidationDisabled);
    const responseValidator = this.buildResponseValidator(methodDocs.responses);

    return async (req: Request, res: Response, next: NextFunction) => {
      try {
        const params = paramsBuilder(req, res, next);
        const response = await handler(...params);

        if (!res.headersSent) {
          if (!responseCode) {
            throw new Error('No success response code described');
          }

          res.status(parseInt(responseCode));

          if (response) {
            responseValidator && !isValidationDisabled && responseValidator(response);
            res.json(response);
          } else {
            res.send();
          }
        }
      } catch (err) {
        return next(err);
      }
    };
  }
}
