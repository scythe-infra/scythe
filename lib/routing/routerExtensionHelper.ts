import { PathItemObject } from 'openapi3-ts'; // @ts-ignore

class ExpressRouterExtension {
  public extend() {
    if (!require('express').isRouterAltered) {
      require('express').isRouterAltered = true;
      require('express').Route.prototype.spec = function(resources: PathItemObject) {
        this._spec = resources;
        return this;
      };
    }
  }
}

export const expressRouterExtension = new ExpressRouterExtension();
