import { IRoute } from 'express-serve-static-core';
import * as express from 'express';
import { PathItemObject } from 'openapi3-ts'; // @ts-ignore

import {
  TOpenAPICallbacks,
  TOpenAPIExamples,
  TOpenAPIHeaders,
  TOpenAPILinks,
  TOpenAPIParameters,
  TOpenAPIRequestBodies,
  TOpenAPIResponses,
  TOpenAPISchemas,
  TOpenAPISecuritySchemas
} from '../openAPI';
import { ExtendedRouter } from '../ScytheApplication';
import { expressRouterExtension } from './routerExtensionHelper';

export interface IOpenAPIRouterInfo {
  schemas: TOpenAPISchemas;
  responses: TOpenAPIResponses;
  parameters: TOpenAPIParameters;
  examples: TOpenAPIExamples;
  requestBodies: TOpenAPIRequestBodies;
  headers: TOpenAPIHeaders;
  securitySchemas: TOpenAPISecuritySchemas;
  links: TOpenAPILinks;
  callbacks: TOpenAPICallbacks;
}

export interface IBuiltRouter {
  expressRouter: ExtendedRouter;
  openAPI: IOpenAPIRouterInfo;
}

export class ScytheRouterBuilder {
  private readonly expressRouter: ExtendedRouter;
  private currentPath?: IRoute & { spec(resources: PathItemObject): IRoute };
  private openAPI: IOpenAPIRouterInfo = {
    schemas: {},
    requestBodies: {},
    responses: {},
    parameters: {},
    examples: {},
    headers: {},
    securitySchemas: {},
    links: {},
    callbacks: {}
  };

  constructor() {
    expressRouterExtension.extend();
    this.expressRouter = express.Router() as ExtendedRouter;
  }

  public useNamespace(namespace: string): ScytheRouterBuilder {
    this.currentPath = this.expressRouter.route(namespace) as IRoute & { spec(resources: PathItemObject): IRoute };
    return this;
  }

  public useOpenAPIDocs(specs: PathItemObject): ScytheRouterBuilder {
    if (!this.currentPath) {
      throw Error("Can't add OpenAPI docs without namespace selected");
    }

    this.currentPath.spec(specs);
    return this;
  }

  public buildNamespace() {
    if (!this.currentPath) {
      throw Error("Can't build namespace without namespace declaration");
    }

    return this.currentPath;
  }

  public setOpenAPIComponents(components: IOpenAPIRouterInfo): ScytheRouterBuilder {
    this.openAPI = components;
    return this;
  }

  public buildRouter(): IBuiltRouter {
    return {
      expressRouter: this.expressRouter,
      openAPI: this.openAPI
    };
  }
}
