export const TAG_CONTROLLER_ENV = Symbol('ControllerEnv');
export const TAG_METHOD_ENV = Symbol('MethodEnv');

export function controllerEnv(...env: string[]): ClassDecorator {
  return function(Controller: Function) {
    Reflect.defineMetadata(TAG_CONTROLLER_ENV, env, Controller);
  };
}

export function methodEnv(...env: string[]): MethodDecorator {
  return function(target, propertyKey) {
    Reflect.defineMetadata(TAG_METHOD_ENV, env, target.constructor, propertyKey);
  };
}
