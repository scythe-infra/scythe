import 'reflect-metadata';

export const TAG_CONTROLLER = Symbol('Controller');

export function controller(path: string, tag?: string): ClassDecorator {
  return function(Controller: Function) {
    const parent = Object.getPrototypeOf(Controller);
    const parentMeta = Reflect.getMetadata(TAG_CONTROLLER, parent);

    if (parentMeta) {
      path = parentMeta.path + path;
      tag = tag || parentMeta.tag;
    }

    Reflect.defineMetadata(TAG_CONTROLLER, { path, tag }, Controller);
  };
}
