import 'reflect-metadata';

export const TAG_BASE_SECURITY = Symbol('BaseSecurity');

export function baseSecurity(securitySchema: string): ClassDecorator {
  return function(Controller: Function) {
    Reflect.defineMetadata(TAG_BASE_SECURITY, securitySchema, Controller);
  };
}
