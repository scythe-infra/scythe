import 'reflect-metadata';
import { buildSchema, createRefContent, getRefObject, ValidationErrorResponse } from '../../openAPI';
import { updateOpenAPIMeta } from './openAPI';

export const TAG_METHOD = Symbol('Method');
export const TAG_METHODS_PARAMS = Symbol('MethodsParams');

export function method(method: string, path: string = '/'): MethodDecorator {
  return function(target: Object, key: string | symbol) {
    const methodsMeta = Reflect.getMetadata(TAG_METHOD, target.constructor) || new Map();
    const pathMeta = methodsMeta.get(path) || new Map();

    if (!methodsMeta.has(path)) {
      methodsMeta.set(path, new Map());
    }

    const stringKey = typeof key === 'string' ? key : key.toString();
    pathMeta.set(method, { key: stringKey, handle: (target as any)[stringKey] });
    methodsMeta.set(path, pathMeta);

    Reflect.defineMetadata(TAG_METHOD, methodsMeta, target.constructor);
  };
}

export const get = (path?: string) => method('get', path);
export const put = (path?: string) => method('put', path);
export const del = (path?: string) => method('delete', path);
export const post = (path?: string) => method('post', path);
export const patch = (path?: string) => method('patch', path);

export enum EMethodParam {
  Body = 'body',
  Query = 'query',
  Path = 'path',
  Req = 'req',
  Res = 'res',
  Next = 'next',
  User = 'user'
}

export interface IMethodParams {
  type: EMethodParam;
  index: number;
  schema?: any;
}

function setMethodsPropertyMeta(
  target: Object,
  propertyKey: string | symbol,
  parameterIndex: number,
  type: EMethodParam,
  schemaName?: string
) {
  const methodsParamsMeta = Reflect.getMetadata(TAG_METHODS_PARAMS, target.constructor, propertyKey) || [];
  const immutableMethodParams = methodsParamsMeta.concat([]);

  immutableMethodParams.push({
    type,
    index: parameterIndex,
    schema: schemaName ? getRefObject(schemaName) : undefined
  });

  Reflect.defineMetadata(TAG_METHODS_PARAMS, immutableMethodParams, target.constructor, propertyKey);
}

export function requestBody(required: boolean = true, contentType?: string): ParameterDecorator {
  return (target: Object, propertyKey: string | symbol, parameterIndex: number) => {
    const params = Reflect.getMetadata('design:paramtypes', target, propertyKey);
    const bodySchema = params[parameterIndex];

    setMethodsPropertyMeta(target, propertyKey, parameterIndex, EMethodParam.Body, bodySchema.name);

    updateOpenAPIMeta(target, propertyKey, operation => {
      operation.requestBody = {
        description: 'Request body',
        required,
        ...createRefContent(bodySchema.name, false, contentType)
      };

      operation.responses = {
        ...operation.responses,
        ...ValidationErrorResponse
      };

      return operation;
    });
  };
}

export function queryParams(target: Object, propertyKey: string | symbol, parameterIndex: number) {
  const params = Reflect.getMetadata('design:paramtypes', target, propertyKey);
  const querySchema = params[parameterIndex];

  setMethodsPropertyMeta(target, propertyKey, parameterIndex, EMethodParam.Query, querySchema.name);

  updateOpenAPIMeta(target, propertyKey, operation => {
    if (!operation.parameters) {
      operation.parameters = [];
    }

    operation.responses = {
      ...operation.responses,
      ...ValidationErrorResponse
    };

    const builtSchema = buildSchema(querySchema);

    for (const property of Object.keys(builtSchema.properties)) {
      operation.parameters.push({
        in: 'query',
        required: builtSchema.required.includes(property),
        name: property,
        schema: builtSchema.properties[property]
      });
    }

    return operation;
  });
}

export function pathParams(target: Object, propertyKey: string | symbol, parameterIndex: number) {
  const params = Reflect.getMetadata('design:paramtypes', target, propertyKey);
  const pathSchema = params[parameterIndex];

  setMethodsPropertyMeta(target, propertyKey, parameterIndex, EMethodParam.Path, pathSchema.name);

  updateOpenAPIMeta(target, propertyKey, operation => {
    if (!operation.parameters) {
      operation.parameters = [];
    }

    operation.responses = {
      ...operation.responses,
      ...ValidationErrorResponse
    };

    const builtSchema = buildSchema(pathSchema);

    for (const property of Object.keys(builtSchema.properties)) {
      operation.parameters.push({
        in: 'path',
        required: builtSchema.required.includes(property),
        name: property,
        schema: builtSchema.properties[property]
      });
    }

    return operation;
  });
}

export function req(target: Object, propertyKey: string, parameterIndex: number) {
  setMethodsPropertyMeta(target, propertyKey, parameterIndex, EMethodParam.Req);
}

export function res(target: Object, propertyKey: string, parameterIndex: number) {
  setMethodsPropertyMeta(target, propertyKey, parameterIndex, EMethodParam.Res);
}

export function next(target: Object, propertyKey: string, parameterIndex: number) {
  setMethodsPropertyMeta(target, propertyKey, parameterIndex, EMethodParam.Next);
}

export function user(target: Object, propertyKey: string, parameterIndex: number) {
  setMethodsPropertyMeta(target, propertyKey, parameterIndex, EMethodParam.User);
}
