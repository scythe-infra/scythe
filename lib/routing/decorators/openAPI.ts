import 'reflect-metadata';
import { OperationObject, ParameterObject } from 'openapi3-ts'; // @ts-ignore
import {
  createRefContent,
  ForbiddenResponse,
  getRefObject,
  getResponse,
  getTypeResponse,
  INewable,
  NoContentResponse,
  NotFoundResponse,
  TSchemaType,
  UnauthorizedResponse,
  ValidationErrorResponse
} from '../../openAPI';

type TOpenAPIMetaHandler = (operation: OperationObject) => OperationObject;

export const TAG_OPENAPI = Symbol('OpenAPI');

export function updateOpenAPIMeta(target: Object, key: string | symbol, handler: TOpenAPIMetaHandler) {
  const specificPathDocks: OperationObject = Reflect.getMetadata(TAG_OPENAPI, target.constructor, key) || { responses: {} };

  Reflect.defineMetadata(TAG_OPENAPI, handler(specificPathDocks), target.constructor, key);
}

export function tag(tag: string): MethodDecorator {
  return function(target: Object, key: string | symbol) {
    updateOpenAPIMeta(target, key, operation => {
      operation.tags = [tag];
      return operation;
    });
  };
}

export function summary(summary: string): MethodDecorator {
  return function(target: Object, key: string | symbol) {
    updateOpenAPIMeta(target, key, operation => {
      operation.summary = summary;
      return operation;
    });
  };
}

export function description(description: string): MethodDecorator {
  return function(target: Object, key: string | symbol) {
    updateOpenAPIMeta(target, key, operation => {
      operation.description = description;
      return operation;
    });
  };
}

export function response(responseCode: number, type: TSchemaType | INewable, isArray: boolean = false): MethodDecorator {
  return function(target: Object, key: string | symbol) {
    updateOpenAPIMeta(target, key, operation => {
      if (typeof type === 'string') {
        operation.responses = {
          ...operation.responses,
          ...getTypeResponse(responseCode.toString(), '', type, isArray)
        };
      } else {
        operation.responses = {
          ...operation.responses,
          ...getResponse(responseCode.toString(), '', createRefContent(type.name, isArray))
        };
      }

      return operation;
    });
  };
}

export enum EDefaultResponse {
  NotFound,
  NoContent,
  Unauthorized,
  Forbidden,
  ValidationError
}

function getDefaultResponse(type: EDefaultResponse) {
  switch (type) {
    case EDefaultResponse.Forbidden:
      return ForbiddenResponse;
    case EDefaultResponse.Unauthorized:
      return UnauthorizedResponse;
    case EDefaultResponse.NoContent:
      return NoContentResponse;
    case EDefaultResponse.NotFound:
      return NotFoundResponse;
    case EDefaultResponse.ValidationError:
      return ValidationErrorResponse;
    default:
      return {};
  }
}

export function defaultResponses(...responses: EDefaultResponse[]): MethodDecorator {
  return function(target: Object, key: string | symbol) {
    updateOpenAPIMeta(target, key, operation => {
      const generatedResponses = responses.reduce((prev, next) => {
        return {
          ...prev,
          ...getDefaultResponse(next)
        };
      }, {});
      operation.responses = {
        ...operation.responses,
        ...generatedResponses
      };

      return operation;
    });
  };
}

export function parameters(...parameters: ParameterObject[]): MethodDecorator {
  return function(target: Object, key: string | symbol) {
    updateOpenAPIMeta(target, key, operation => {
      if (!operation.parameters) {
        operation.parameters = [];
      }

      operation.responses = {
        ...operation.responses,
        ...ValidationErrorResponse
      };

      operation.parameters = operation.parameters.concat(parameters);
      return operation;
    });
  };
}

export function headerParameter(name: string, schema: TSchemaType | INewable, required: boolean = true): MethodDecorator {
  return function(target: Object, key: string | symbol) {
    updateOpenAPIMeta(target, key, operation => {
      if (!operation.parameters) {
        operation.parameters = [];
      }

      operation.parameters.push({
        in: 'header',
        required,
        name,
        schema: typeof schema === 'string' ? { type: schema } : getRefObject(schema.name)
      });
      return operation;
    });
  };
}

export function security(name: string): MethodDecorator {
  return function(target: Object, key: string | symbol) {
    updateOpenAPIMeta(target, key, operation => {
      if (!operation.security) {
        operation.security = [];
      }

      operation.responses = {
        ...operation.responses,
        ...UnauthorizedResponse
      };

      operation.security.push({
        [name]: []
      });
      return operation;
    });
  };
}

export function deprecated(): MethodDecorator {
  return function(target: Object, key: string | symbol) {
    updateOpenAPIMeta(target, key, operation => {
      operation.deprecated = true;
      return operation;
    });
  };
}
