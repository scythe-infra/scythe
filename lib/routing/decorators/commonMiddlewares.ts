import 'reflect-metadata';
import { RequestHandler } from 'express-serve-static-core';

export const TAG_COMMON_MIDDLEWARES = Symbol('CommonMiddlewares');

export function commonMiddlewares(...middlewares: RequestHandler[]): ClassDecorator {
  return function(Controller: Function) {
    const parent = Object.getPrototypeOf(Controller);
    const parentMeta = Reflect.getMetadata(TAG_COMMON_MIDDLEWARES, parent);

    if (parentMeta) {
      middlewares = parentMeta.concat(middlewares);
    }

    Reflect.defineMetadata(TAG_COMMON_MIDDLEWARES, middlewares, Controller);
  };
}
