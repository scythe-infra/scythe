import 'reflect-metadata';
import { RequestHandler } from 'express-serve-static-core';

export const TAG_MIDDLEWARES = Symbol('Middlewares');

export function middlewares(...middlewares: RequestHandler[]): MethodDecorator {
  return function(target: Object, key: string | symbol) {
    Reflect.defineMetadata(TAG_MIDDLEWARES, middlewares, target.constructor, key);
  };
}
