import 'reflect-metadata';

export const TAG_DISABLE_VALIDATION = Symbol('DisableValidation');

export function disableValidation(target: Object, key: string | symbol) {
  Reflect.defineMetadata(TAG_DISABLE_VALIDATION, true, target.constructor, key);
}
