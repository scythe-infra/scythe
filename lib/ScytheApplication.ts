import 'reflect-metadata';

import { ErrorRequestHandler, Express, RequestHandler, Request, Response, NextFunction, Router } from 'express';
import * as express from 'express';
import * as http from 'http';
import * as bodyParser from 'body-parser';
import * as log4js from 'log4js';
import * as path from 'path';
import * as fs from 'fs';
import * as swaggerUi from 'swagger-ui-express';
import { PathParams } from 'express-serve-static-core';
import { AbstractScytheApplication, Module, TJobHandler } from '@scythe/types';
import { InfoObject, PathItemObject } from 'openapi3-ts'; // @ts-ignore
import { Socket, Server as WebSocketServer, ServerOptions } from 'socket.io';
import * as socketIO from 'socket.io';

import { OpenAPI, SchemaValidator, SCHEMAS_META, buildSchema } from './openAPI';
import { scheduler, SchedulerModule } from './scheduler';
import { IBuiltRouter, expressRouterExtension, ScytheControllerBuilder } from './routing';
import { TAG_CONTROLLER } from './routing/decorators';
import { HttpError } from './HttpError';
import { SocketService, TAG_WS_CONTROLLER, TEventHandler, WebSocketEventBuilder } from './WebSockets';
import { ScytheLogger } from './logging';

export { Request, Response, NextFunction };
export { Socket, WebSocketServer };

export interface IConfig {
  withoutServer?: boolean;
  validateRequests?: boolean;
  validateResponse?: boolean;
  hydrateOpenAPIModels?: boolean;
  openAPIInfo?: InfoObject;
  openAPIDocksEndpoint?: string;
  aclPath?: string;
  enableLogsForPM2?: boolean;
  socketIOOptions?: ServerOptions;
  [key: string]: any;
}

export type ExtendedRouter = Router & IExtendedRouter;

export interface IApplicationJob {
  schedule: string;
  job: TJobHandler;
}

export interface IApplicationRoute {
  url: PathParams;
  builtRouter: IBuiltRouter;
}

interface IExtendedRouter {
  spec: (resources: PathItemObject) => ExtendedRouter;
  _spec: PathItemObject;
}

export class ScytheApplication extends AbstractScytheApplication {
  public server?: Express;
  public httpServer?: http.Server;
  public openAPI = new OpenAPI();
  public socketIOServer?: WebSocketServer;

  private connection?: http.Server;
  private routers: IApplicationRoute[] = [];
  private controllerBuilders: (ScytheControllerBuilder | WebSocketEventBuilder)[] = [];
  private modules: Module[] = [];
  private jobs: IApplicationJob[] = [];
  private logger = log4js.getLogger('Application');

  constructor(private config: IConfig, private logPath: string, private additionalMiddlewares: RequestHandler[] = []) {
    super();

    this.addModule(new SchedulerModule());

    if (!this.config.withoutServer) {
      expressRouterExtension.extend();

      this.openAPI.addOpenApiVersion('3.0.2');
    }
  }

  public addRouter(url: PathParams, router: IBuiltRouter): void {
    this.routers.push({
      url: url,
      builtRouter: router
    });
  }

  public addModule<T extends Module>(module: T) {
    this.modules.push(module);
  }

  public async start(errorHandler?: ErrorRequestHandler) {
    this.configureLogs(this.logPath);

    for (const module of this.modules) {
      await module.initialize(this);
    }

    this.initJobs();

    if (!this.config.withoutServer) {
      this.useDefaultOpenAPISchemas();
      await this.initServer();
      this.errorHandling(errorHandler);

      const port = parseInt(process.env.PORT || '');
      await this.startServer(isNaN(port) ? 3030 : port);
    }
    this.registerShutdownHooks();
  }

  public addJob(schedule: string, job: TJobHandler) {
    this.jobs.push({
      schedule,
      job
    });
  }

  public async stop(): Promise<void> {
    const modulesStopPromises = this.modules.map(module => module.stop(this));
    await Promise.all(modulesStopPromises);

    this.routers = [];
    this.openAPI = new OpenAPI();
    this.server = undefined;

    if (this.connection) {
      return new Promise((resolve, reject) => {
        this.connection!.close((err?: Error) => {
          err ? reject(err) : resolve();
        });
      });
    }
  }

  public getOpenAPIDocs() {
    return this.openAPI.getSpec();
  }

  public registerController(Controller: any, basePath?: string) {
    const isWSController = Reflect.hasMetadata(TAG_WS_CONTROLLER, Controller);
    const isHttpController = Reflect.hasMetadata(TAG_CONTROLLER, Controller);
    if (isHttpController) {
      this.controllerBuilders.push(new ScytheControllerBuilder(Controller, basePath));
    } else if (isWSController) {
      this.controllerBuilders.push(new WebSocketEventBuilder(Controller));
    } else {
      this.logger.info('Cannot parse controller');
    }
  }

  private useDefaultOpenAPISchemas() {
    this.openAPI
      .addSchema('SwaggerValidationErrorDetail', {
        type: 'object',
        required: ['dataPath', 'message', 'params'],
        properties: {
          dataPath: {
            type: 'string',
            description: 'path in validated object'
          },
          message: {
            type: 'string',
            description: 'Swagger validation error message'
          },
          params: {
            type: 'object',
            description: 'additional information about error'
          }
        }
      })
      .addSchema('ValidationError', {
        type: 'object',
        required: ['error'],
        properties: {
          error: {
            type: 'object',
            required: ['message', 'details'],
            properties: {
              message: {
                type: 'string',
                description: 'Error message'
              },
              details: {
                type: 'array',
                items: {
                  $ref: '#/components/schemas/SwaggerValidationErrorDetail'
                }
              }
            }
          }
        }
      })
      .addSchema('BaseError', {
        type: 'object',
        required: ['error'],
        properties: {
          error: {
            type: 'object',
            required: ['message'],
            properties: {
              message: {
                type: 'string',
                description: 'Error message'
              }
            }
          }
        }
      });

    this.loadDocsFromSchemas();
  }

  private initJobs() {
    this.jobs.forEach(definition => {
      scheduler.scheduleJob(definition.schedule, definition.job);
    });
  }

  private async initServer() {
    this.server = express();
    this.httpServer = http.createServer(this.server);

    await SchemaValidator.init(this.openAPI);
    this.initControllers();
    this.generateSwaggerDocs();

    this.declareMiddlewares();
    await this.initSwagger();
    this.initRoutes();
  }

  private initControllers() {
    let wsEvents = new Map<string, TEventHandler>();

    for (const builder of this.controllerBuilders) {
      if (builder instanceof ScytheControllerBuilder) {
        const router = builder
          .setConfig(this.config)
          .setOpenAPI(this.openAPI)
          .build();
        this.addRouter('/', router);
      } else {
        const controllerEvents = builder.setLogger(this.logger).build(wsEvents);
        wsEvents = new Map([...wsEvents, ...controllerEvents]);
      }
    }

    this.initWebSocketAPI(wsEvents);
  }

  private loadDocsFromSchemas() {
    const schemasMeta = Reflect.getMetadata(SCHEMAS_META, OpenAPI) || new Map();

    for (const [name, schema] of schemasMeta) {
      this.openAPI.addSchema(name, buildSchema(schema));
    }
  }

  private initWebSocketAPI(events: Map<string, TEventHandler>) {
    if (!this.httpServer) {
      throw new Error('No HTTP server detected');
    }

    if (events.size) {
      this.socketIOServer = socketIO(this.httpServer, this.config.socketIOOptions);
      SocketService.init(this.socketIOServer);
      this.logger.info('WebSocket server connected');

      this.socketIOServer.on('connection', socket => {
        for (let [key, handler] of events) {
          socket.on(key, message => handler(message, socket, this.socketIOServer!));
        }
      });
    }
  }

  private initRoutes() {
    this.routers.forEach(definition => {
      this.server!.use(definition.url, definition.builtRouter.expressRouter);
    });
  }

  private declareMiddlewares() {
    this.server!.use((req: any, res, next) => {
      req.rawBody = Buffer.from('');

      req.on('data', (chunk: any) => {
        let prev = req.rawBody;
        let length = prev.length + chunk.length;
        req.rawBody = Buffer.concat([prev, chunk], length);
      });

      next();
    });

    (this.additionalMiddlewares || []).forEach(middleware => this.server!.use(middleware));
    this.additionalMiddlewares = [];

    this.server!.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
    this.server!.use(bodyParser.json({ limit: '50mb' }));
    this.server!.use(bodyParser.text({ limit: '50mb' }));

    process.on('uncaughtException', err => {
      this.logger.error('Unhandled exception', err);
    });
    process.on('unhandledRejection', err => {
      this.logger.error('Unhandled rejection', err);
    });
  }

  private configureLogs(pathToFile: string): void {
    let _path = path.dirname(pathToFile);

    if (!fs.existsSync(_path)) {
      fs.mkdirSync(_path);
    }

    log4js.addLayout('json', function(config) {
      return function(logEvent) {
        const resultLogEvent = {
          ...logEvent,
          data: logEvent.data[0].filePath
            ? logEvent.data[0]
            : logEvent.data.reduce((accum, item, index) => {
                const key = index === 0 ? 'message' : `detail ${index}`;
                return { ...accum, [key]: item };
              }, {})
        };

        return ScytheLogger.stringify(resultLogEvent) + config.separator;
      };
    });

    let appenders: any = {
      app: { type: 'file', filename: pathToFile, timezoneOffset: 0, layout: { type: 'json', separator: ',' } }
    };
    let appendersList = ['app'];

    if (process.env.NODE_ENV !== 'production') {
      appenders.debug = { type: 'console' };
      appendersList.push('debug');
    }

    log4js.configure({
      pm2: !!this.config.enableLogsForPM2,
      categories: {
        default: {
          appenders: appendersList,
          level: process.env.LOG_LEVEL || 'all'
        }
      },
      appenders: appenders
    });
  }

  private startServer(port: number): Promise<void> {
    return new Promise(resolve => {
      this.connection = this.httpServer!.listen(port, () => {
        this.logger.info(`Express server connected on ${port}`);
        resolve();
      });
    });
  }

  private registerShutdownHooks() {
    let gracefulShutdown = () =>
      this.stop()
        .then(() => this.logger.info('Stopping service'))
        .then(() => process.exit(), () => process.exit());

    process.on('SIGTERM', gracefulShutdown);
    process.on('SIGINT', gracefulShutdown);
  }

  private errorHandling(customErrorHandler?: ErrorRequestHandler) {
    this.server!.use((req, res, next) => {
      let err = new Error(`Can't find path ${req.originalUrl}`) as Error & { status: number };
      err.status = 404;
      next(err);
    });

    const errorHandler: ErrorRequestHandler = (err, req, res, next) => {
      const result = customErrorHandler ? customErrorHandler(err, req, res, next) || err : err;

      if (result instanceof HttpError) {
        this.logger.error('Request error', JSON.stringify(result.error), result.stack);
        res.status(result.status);
        return res.json(result.error);
      } else {
        if (result.code) {
          result.status = result.code;
        }

        if (!result.status) {
          result.status = 500;
        }

        res.status(result.status);

        if (result.status === 500 && req.app.get('env') === 'production') {
          result.message = 'INTERNAL_SERVER_ERROR';
        }

        this.logger.error('Request error', result);
        return res.json(result);
      }
    };
    this.server!.use(errorHandler);
  }

  private generateSwaggerDocs(config: IConfig = this.config) {
    if (config.swaggerInfo) {
      this.openAPI.addInfo(config.swaggerInfo);
    }

    this.routers.forEach(definition => {
      this.openAPI.readComponents(definition.builtRouter.openAPI);
    });

    this.routers.forEach(router => {
      this.openAPI.readResources(router.builtRouter.expressRouter, router.url);
    });
  }

  private async initSwagger() {
    const openAPISpec = this.openAPI.getSpec();

    this.server!.use(this.config.swaggerDocksEndpoint || '/swagger', swaggerUi.serve, swaggerUi.setup(openAPISpec));
  }
}
