'use strict';

import { scheduler } from './scheduler';
import 'should';

describe('Scheduler', () => {
  it('should schedule and stop jobs', done => {
    let count = 0;
    let callback = () => count++;
    scheduler.scheduleJob('* * * * * *', callback);
    setTimeout(() => {
      count.should.eql(2);
      scheduler.stopJobs();
      setTimeout(() => {
        count.should.eql(2);
        done();
      }, 1100);
    }, 2100);
  });
});
