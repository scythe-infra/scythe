'use strict';

import * as schedule from 'node-schedule';

/**
 * Job scheduler
 */
export class Scheduler {
  private jobs: schedule.Job[];
  /**
   * Constructs job scheduler
   */
  constructor() {
    this.jobs = [];
  }

  /**
   * Schedules a job
   * @param {Object} jobSchedule job schedule
   * @param {Function} callback job callback
   * @returns {Object} job instance
   */
  scheduleJob(jobSchedule: string, callback: () => any) {
    let job = schedule.scheduleJob(jobSchedule, callback);
    this.jobs.push(job);
    return job;
  }

  /**
   * Stops all running jobs
   */
  stopJobs() {
    this.jobs.forEach(job => job.cancel());
    this.jobs = [];
  }
}

export const scheduler = new Scheduler();
