'use strict';

import { AbstractScytheApplication, Module } from '@scythe/types';
import { scheduler } from './scheduler';

/**
 * This module enables a task scheduling feature
 */
export class SchedulerModule extends Module {
  constructor() {
    super();
  }

  public stop<T extends AbstractScytheApplication>(app: T) {
    scheduler.stopJobs();
  }
}
