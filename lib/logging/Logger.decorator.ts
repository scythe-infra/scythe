import { ScytheLogger } from './ScytheLogger';

export function log(fn: Function, key: string, descriptor: any) {
  const { LOG_LEVEL } = process.env;

  if (LOG_LEVEL === 'debug' || LOG_LEVEL === 'all') {
    return {
      value: async function(...args: any[]) {
        const arg = args
          .map(arg => {
            const res = JSON.stringify(arg) || '';
            return res.slice(0, 150);
          })
          .join();

        const callId = Math.random()
          .toString()
          .slice(2, 8);

        const stack = new Error().stack || '';
        const filePath = `...${stack.split('\n')[7].slice(-35, -6)}`;
        const startTime = Date.now();

        try {
          ScytheLogger.system(filePath, `${fn.name}.${key}`, callId, 'START', 0, arg);

          const result = await descriptor.value.apply(this, args);

          const executionTime = Date.now() - startTime;
          const resString = (ScytheLogger.stringify(result) || '').slice(0, 150);

          ScytheLogger.system(filePath, `${fn.name}.${key}`, callId, 'SUCCESS', executionTime, resString);

          return result;
        } catch (e) {
          const executionTime = Date.now() - startTime;

          ScytheLogger.system(filePath, `${fn.name}.${key}`, callId, 'FAIL', executionTime, e, arg, e.stack);
          throw e;
        }
      }
    };
  } else {
    return descriptor;
  }
}

export function Log<TFunction extends Function>(target: TFunction): TFunction {
  Object.getOwnPropertyNames(target).map(fn => {
    if (Object.getOwnPropertyDescriptor(target, fn)!.value instanceof Function)
      return Object.defineProperty(target, fn, log(target, fn, Object.getOwnPropertyDescriptor(target, fn)));
  });
  return target;
}
