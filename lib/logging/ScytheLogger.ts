import * as log4js from 'log4js';
import { Logger } from 'log4js';

export class ScytheLogger {
  public static logger: Logger = log4js.getLogger('Application');

  public static system(
    filePath: string,
    origin: string,
    callId: string,
    operationStage: string,
    executionTime: number,
    msg: string,
    params?: string,
    stack?: string
  ) {
    this.logger.debug({ filePath, origin, callId, operationStage, executionTime, msg, params, stack });
  }

  public static stringify(o: any) {
    const cache: any[] = [];
    const res = JSON.stringify(o, function(key, value) {
      if (typeof value === 'object' && value !== null) {
        if (cache.includes(value)) {
          return;
        }
        cache.push(value);
      }
      return value;
    });
    return res;
  }
}
