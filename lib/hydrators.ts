import { buildSchema, getSchemaName, OpenAPI, SCHEMAS_META } from './openAPI';

export function getSchemaHydrator(schemaRef: any) {
  if (!schemaRef) {
    return undefined;
  }
  return (data: any) => schemaHydrator(data, schemaRef);
}

function schemaHydrator(body: any, refObject: any) {
  const schemasMeta = Reflect.getMetadata(SCHEMAS_META, OpenAPI) || new Map();

  const schemaName = getSchemaName(refObject);
  const RequestBodySchema = schemasMeta.get(schemaName);

  if (!RequestBodySchema) {
    throw new Error(`You must define ${schemaName} schema`);
  }

  const builtSchema = buildSchema(RequestBodySchema);
  const properties = builtSchema.properties;

  const model = new RequestBodySchema();

  for (const key of Object.keys(properties)) {
    const docsProp = properties[key];

    let hydaratedValue: any;
    switch (docsProp.type) {
      case 'number':
      case 'integer':
        hydaratedValue = hydrateValue(key, body, Number);
        break;

      case 'string':
        hydaratedValue = hydrateValue(key, body, String);
        break;

      case 'boolean':
        hydaratedValue = hydrateValue(key, body, Boolean);
        break;
      case 'object':
        hydaratedValue = body[key];
        break;
      case 'array':
        hydaratedValue = hydrateArray(key, body, docsProp.items);
        break;
      default:
        hydaratedValue = hydrateValue(key, body, (result: any) => schemaHydrator(result, docsProp));
        break;
    }

    if (hydaratedValue !== undefined) {
      model[key] = hydaratedValue;
    }
  }

  return model;
}

function hydrateArray(key: string, body: any, itemsDocs: any) {
  if (body.hasOwnProperty(key)) {
    switch (itemsDocs.type) {
      case 'number':
      case 'integer':
        return body[key].map(Number);
      case 'string':
        return body[key].map(String);
      case 'boolean':
        return body[key].map(Boolean);
      default:
        return body[key].map((item: any) => schemaHydrator(item, itemsDocs));
    }
  }
}

function hydrateValue(key: string, body: any, hydrator: Function) {
  if (body.hasOwnProperty(key)) {
    return hydrator(body[key]);
  }

  return undefined;
}
