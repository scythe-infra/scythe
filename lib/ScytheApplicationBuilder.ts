import { IApplicationJob, IApplicationRoute, IConfig, ScytheApplication } from './ScytheApplication';
import { IBuiltRouter } from './routing';
import { Module, TJobHandler } from '@scythe/types';
import { RequestHandler } from 'express';
import { PathParams } from 'express-serve-static-core';

interface IController {
  controller: any;
  baseUrl?: string;
}

export class ScytheApplicationBuilder {
  private config: IConfig = {
    validateResponse: true,
    validateRequests: true,
    hydrateOpenAPIModels: true
  };
  private logPath: string = './logs/application.log';
  private routers: IApplicationRoute[] = [];
  private modules: Module[] = [];
  private jobs: IApplicationJob[] = [];
  private controllers: IController[] = [];
  private additionalMiddlewares: RequestHandler[] = [];

  public setConfig(config: IConfig) {
    this.config = config;
    return this;
  }

  public setLogPath(path: string) {
    this.logPath = path;
    return this;
  }

  public addRouter(url: PathParams, router: IBuiltRouter) {
    this.routers.push({
      url,
      builtRouter: router
    });
    return this;
  }

  public addModule<T extends Module>(module: T) {
    this.modules.push(module);
    return this;
  }

  public addAdditionalMiddleware(middleware: RequestHandler) {
    this.additionalMiddlewares.push(middleware);
    return this;
  }

  public addJob(schedule: string, job: TJobHandler) {
    this.jobs.push({
      schedule,
      job
    });
    return this;
  }

  public addController(Controller: any, basePath?: string) {
    this.controllers.push({
      controller: Controller,
      baseUrl: basePath
    });
    return this;
  }

  public build(): ScytheApplication {
    const app = new ScytheApplication(this.config, this.logPath, this.additionalMiddlewares);

    this.routers.forEach(router => {
      app.addRouter(router.url, router.builtRouter);
    });

    this.modules.forEach(module => {
      app.addModule(module);
    });

    this.jobs.forEach(job => {
      app.addJob(job.schedule, job.job);
    });

    this.controllers.forEach(controller => {
      app.registerController(controller.controller, controller.baseUrl);
    });

    return app;
  }
}
